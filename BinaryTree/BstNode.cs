﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    /// <summary>
    /// класс узла бинарного дерева поиска
    /// </summary>
    internal class BstNode<T> where T : IComparable<T>
    {
        public BstNode(T data)
        {
            Data = data;
        }


        public T Data { get; set; }
        public BstNode<T>? LeftNode { get; set; }
        public BstNode<T>? RightNode { get; set; }


        public override string? ToString() => Data.ToString();

    }
}
