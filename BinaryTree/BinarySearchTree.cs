﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    /// <summary>
    /// Класс бинарного дерева поиска
    /// </summary>
    internal class BinarySearchTree<T> where T : IComparable<T>
    {
        public BinarySearchTree(BstNode<T>? rootNode = null)
        {
            RootNode = rootNode;
        }
        public BinarySearchTree(T rootData)
        {
            RootNode = new BstNode<T>(rootData);
        }


        public BstNode<T>? RootNode { get; set; }


        /// <summary>
        /// Добавление узла в дерево
        /// </summary>
        public void Add(BstNode<T> nodeToAdd, BstNode<T>? baseNode = null)
        {
            if (RootNode == null)
            {
                RootNode = nodeToAdd;
                return;
            }

            baseNode ??= RootNode;

            // повторяющиеся значения будем отправлять в правое поддерево
            if (nodeToAdd.Data.CompareTo(baseNode.Data) >= 0)
            {
                if (baseNode.RightNode is not null)
                    Add(nodeToAdd, baseNode.RightNode);
                else
                    baseNode.RightNode = nodeToAdd;
            }
            else
            {
                if (baseNode.LeftNode is not null)
                    Add(nodeToAdd, baseNode.LeftNode);
                else
                    baseNode.LeftNode = nodeToAdd;
            }
        }

        /// <summary>
        /// Перегрузка для удобства
        /// </summary>
        public void Add(T data, BstNode<T>? baseNode = null)
        {
            Add(new BstNode<T>(data), baseNode);
        }


        /// <summary>
        /// поиск первого вхождения узла с указанной информацией. Если не находит, возвращает null
        /// </summary>
        public BstNode<T>? Find(T data, BstNode<T>? baseNode = null)
        {
            if (RootNode == null) return null;

            baseNode ??= RootNode;

            if (data.CompareTo(baseNode.Data) > 0)
            {
                if (baseNode.RightNode is not null)
                    return Find(data, baseNode.RightNode);
                else
                    return null;
            }
            else if (data.CompareTo(baseNode.Data) < 0)
            {
                if (baseNode.LeftNode is not null)
                    return Find(data, baseNode.LeftNode);
                else
                    return null;
            }
            else 
                return baseNode;
        }


        /// <summary>
        /// Поиск всех узлов с указанной информацией (допустимы повторяющиеся узлы)
        /// </summary>
        public List<BstNode<T>>? FindAll(T data, BstNode<T>? baseNode = null) 
        {
            if (RootNode == null) return null;

            baseNode ??= RootNode;

            BstNode<T>? foundNode = Find(data, baseNode); 

            if (foundNode is null) return null;

            List<BstNode<T>> results = new() { foundNode };
            // пытаемся найти повторяющиеся значения в правой ветке от найденного
            do
            {
                if (foundNode.RightNode is null) break;

                foundNode = Find(data, foundNode.RightNode);
                if (foundNode is not null)
                    results.Add(foundNode);
            } while (foundNode is not null);

            return results;
        }


        /// <summary>
        /// симметричный обход дерева
        /// </summary>
        public void Traverse (BstNode<T>? baseNode = null)
        {
            if (RootNode == null) return;
            baseNode ??= RootNode;

            if (baseNode.LeftNode is not null)
            {
                Traverse(baseNode.LeftNode);
            }

            Console.WriteLine(baseNode.ToString());

            if (baseNode.RightNode is not null)
            {
                Traverse(baseNode.RightNode);
            }
        }

    }
}
