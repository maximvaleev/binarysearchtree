﻿
/*
 * Домашнее задание

Деревья и кучи

Напишите программу, которая:

*  принимает на вход из консоли информацию о сотрудниках: имя + зарплата (имя в первой строке, 
   зарплата в виде целого числа во второй строке; и так для каждого сотрудника, пока пользователь 
    не введет пустую строку в качестве имени сотрудника)
*  попутно при получении информации о сотрудниках строится бинарное дерево с этой информацией, 
    где в каждом узле хранится имя сотрудника, а его зарплата является значением, 
    на основе которого производится бинарное разделение в дереве
*  после окончания ввода пользователем программа выводит имена сотрудников и их зарплаты 
    в порядке возрастания зарплат (в каждой строчке формат вывода "Имя - зарплата"). 
    Использовать для этого симметричный обход дерева.
*  после этого программа запрашивает размер зарплаты, который интересует пользователя. 
    В построенном бинарном дереве программа находит сотрудника с указанной зарплатой и выводит его имя. 
    Если сотрудник не найден - выводится "такой сотрудник не найден"
*  после этого программа предлагает ввести цифру 0 (переход к началу программы) или 1 (снова поиск зарплаты). 
    При вводе 0 должен произойти переход к началу работы программы, т.е. опять запрашивается список сотрудников 
    и строится новое дерево. При вводе 1 должны снова запросить зарплату, 
    которую хочется поискать в дереве - см.предыдущий пункт.


Критерии оценки:

Построение бинарного дерева: +2 балла
Вывод сотрудников, отсортированный по их зарплате: +3 балла
Поиск сотрудника с искомой зарплатой: +3 балла.
Реализован интерактив (запрос списка сотрудников и их зарплат, запрос искомой зарплаты, запрос 0 или 1 в конце): +2 балла
Если все значения "зашиты" в программу: +1 балл
Для сдачи достаточно 6 баллов.

Рекомендуем сдать до: 03.08.2023
*/

namespace BinaryTree
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\nПривет! Давай протестируем бинарное дерево поиска.\n");

            do // повторять программу по желанию пользователя
            {
                BinarySearchTree<Employee> tree = GetData();

                Console.WriteLine("\nОбход дерева:\n");
                tree.Traverse();

                SearchBySalaries(tree);
            } while (ConsoleHelpers.AskUserForZeroOrOne("Ввести новое дерево?") == 1);

        }

        
        /// <summary>
        /// Создать дерево. Пользователь выбирает вручную или из заготовки
        /// </summary>
        private static BinarySearchTree<Employee> GetData()
        {
            if (ConsoleHelpers.AskUserForZeroOrOne("Как ввести данные?", "вручную", "загрузить заготовленные") == 0)
                return GetManualInputData();
            else
                return GetPrefabricatedData();
        }


        /// <summary>
        /// Создать дерево из заготовки
        /// </summary>
        private static BinarySearchTree<Employee> GetPrefabricatedData()
        {
            BinarySearchTree<Employee> tree = new();

            tree.Add(new Employee("Абрикосов", 200));
            tree.Add(new Employee("Авокадова", 350));
            tree.Add(new Employee("Арбузина", 320));
            tree.Add(new Employee("Урожайный", 190));
            tree.Add(new Employee("Сладкий", 200));
            tree.Add(new Employee("Сезонный", 220));
            tree.Add(new Employee("Вишневский", 360));
            tree.Add(new Employee("Лимонова", 400));
            tree.Add(new Employee("Цитрусов", 230));
            tree.Add(new Employee("Косточкин", 380));
            tree.Add(new Employee("Кожуркина", 230));
            tree.Add(new Employee("Бананистый", 300));
            tree.Add(new Employee("Сочная", 230));

            return tree;
        }


        /// <summary>
        /// Создать дерево, вводя данные в консоль вручную
        /// </summary>
        private static BinarySearchTree<Employee> GetManualInputData()
        {
            Console.WriteLine("\nРучной ввод данных о сотрудниках.");
            Console.WriteLine("(для завершения ввода не вводите имя сотрудка и нажмите Enter)");

            BinarySearchTree<Employee> tree = new();

            for (int employeeNumber = 1; ; employeeNumber++)
            {
                Console.Write($"\nВведите имя сотрудника №{employeeNumber}: ");
                string? name = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(name)) break;

                int salary = ConsoleHelpers.AskUserForInt($"Введите зарплату сотрудника №{employeeNumber}: ", true, false);

                tree.Add(new Employee(name, salary));
            }

            return tree;
        }


        /// <summary>
        /// Искать в дереве узлы по зарплатам.
        /// </summary>
        private static void SearchBySalaries(BinarySearchTree<Employee> tree)
        {
            Console.WriteLine("\nДавайте найдем сотрудника по его зарплате.");

            do
            {
                int salary = ConsoleHelpers.AskUserForInt("Введите зарплату для поиска: ", true, false);

                // Поскольку зарплаты могут повторяться, ищем все узлы с этим значением
                List<BstNode<Employee>>? searchResults = tree.FindAll(new Employee(string.Empty, salary));

                if (searchResults == null)
                {
                    Console.WriteLine("\nСотрудников с такой зарплатой нет.");
                }
                else
                {
                    if (searchResults.Count == 1)
                    {
                        Console.WriteLine("\nНайден сотрудник: " + searchResults[0].ToString());
                    }
                    else
                    {
                        Console.WriteLine("\nНайдено несколько сотрудников с такой зарплатой:");
                        foreach (BstNode<Employee> employee in searchResults)
                        {
                            Console.WriteLine(employee.ToString());
                        }
                    }
                }
            } while (ConsoleHelpers.AskUserForZeroOrOne("Повторить поиск?") == 1);
            
        }
    }
}