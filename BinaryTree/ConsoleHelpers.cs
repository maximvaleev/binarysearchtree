﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    /// <summary>
    /// обертки для работы с консолью
    /// </summary>
    public static class ConsoleHelpers
    {
        /// <summary>
        /// Попросить пользователя ввести целочисленное значение
        /// </summary>
        public static int AskUserForInt(string message = "Введите число: ", bool acceptZero = true, bool acceptNegative = true)
        {
            Console.Write("\n" + message);

            string? inputStr;
            int inputInt;
            bool isFirstTime = true;
            do
            {
                if (!isFirstTime)
                    Console.Write("\tневерный ввод! попробуйте еще раз: ");
                isFirstTime = false;
                inputStr = Console.ReadLine();
            } while (!int.TryParse(inputStr, out inputInt) || (!acceptZero && inputInt == 0) || (!acceptNegative && inputInt < 0));

            return inputInt;
        }

        /// <summary>
        /// Попросить пользователя ввести 1 или 0
        /// </summary>
        public static int AskUserForZeroOrOne(string? message = null, string answerForZero = "нет", string answerForOne = "да")
        {
            if (message != null)
                Console.WriteLine("\n" + message);
            Console.Write($"\t0 - {answerForZero} / 1 - {answerForOne}: ");

            char input;
            do
            {
                input = Console.ReadKey(intercept: true).KeyChar;
            } while (!Char.IsDigit(input) || Char.GetNumericValue(input) > 1.1);
            Console.WriteLine(input);

            return Convert.ToInt32(Char.GetNumericValue(input));
        }

    }
}
