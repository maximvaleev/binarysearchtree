﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    /// <summary>
    /// Класс для хранения данных в узлах бинарного дерева
    /// </summary>
    internal class Employee: IComparable<Employee>
    {
        public Employee(string name, int salary)
        {
            Name = name;
            Salary = salary;
        }

        public string Name { get; set; }
        public int Salary { get; set; }

        public int CompareTo(Employee? other)
        {
            if (other == null) 
                throw new ArgumentNullException(nameof(other), "Can't compare with null");
            
            if (this.Salary > other.Salary) 
                return 1;
            else if (this.Salary < other.Salary)
                return -1;
            else
                return 0;
        }
        
        public override string ToString()
        {
            // для смены культуры на другую, если вдруг
            //System.Globalization.CultureInfo.CurrentCulture = new System.Globalization.CultureInfo("ru-RU", false);
            // для отображения символа рубля в консоли
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            return $"{Salary:C0} - {Name}";
        }
    }
}
